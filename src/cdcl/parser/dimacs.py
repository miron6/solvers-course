from cnf.cnf import ConjunctiveNormalForm

class ParserError(Exception):
    '''Custom exceptions handler for DIMACS parser'''

class DIMACS:
    _file_content: list
    _comment: str

    _count_vars: int
    _vars: set[int]

    _count_clauses: int
    _clauses: set[frozenset[int]]

    def __init__(self: 'DIMACS', filename: str) -> None:
        self._file_content = []
        with open(filename, 'r', encoding='utf-8') as f:
            for line in f.readlines():
                self._file_content.append(line.strip())
        if len(self._file_content) < 3:
            raise ParserError('Error while parsing file. No clauses found.')

    def parse(self: 'DIMACS') -> 'ConjunctiveNormalForm':
        self._comment = self._file_content[0]
        self._parse_header()
        self._parse_clauses()
        return ConjunctiveNormalForm(self._clauses, self._vars, self._count_vars, self._count_clauses)

    def _parse_header(self: 'DIMACS') -> None:
        tokens = self._file_content[1].split(' ')
        self._count_vars = int(tokens[2])
        self._count_clauses = int(tokens[3])

    def _parse_clauses(self: 'DIMACS') -> None:
        self._clauses = set()
        self._vars = set()

        for clause in self._file_content[2:]:
            if clause[-1] != '0':
                raise ParserError('Error while parsing file. Each clause should ended with 0')
            parsed_clause = frozenset(map(int, clause[:-1].split()))
            self._vars.update(map(abs, parsed_clause))
            self._clauses.add(parsed_clause)
        if len(self._vars) != self._count_vars or len(self._clauses) != self._count_clauses:
            raise ParserError(
                'Error while parsing file. Missmatched literals or clauses counter.'
                '   Literals expected: {}, have: {}'
                '   Clauses expected: {}, have: {}'.format(
                    self._count_vars, len(self._vars), self._count_clauses, len(self._clauses)
                ))

    def __str__(self: 'DIMACS'):
        cls = ''
        for clause in self._clauses:
            oneline = ''
            for item in clause:
                if clause[item] == 0:
                    oneline += f'-{item} '
                else:
                    oneline += item + ' '
            cls += f'{oneline}0\n'
        cls = cls.rstrip('\n')
        return f'{self._comment}\np cnf {self._count_vars}{self._count_clauses}\n{cls}'

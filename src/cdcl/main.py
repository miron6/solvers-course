from argparse import ArgumentParser

from parser.dimacs import DIMACS
from solver.cdcl import CDCLSolver

def main(filename):
    dimacs_parser = DIMACS(filename)
    cnf = dimacs_parser.parse()
    solver = CDCLSolver(cnf)
    solver.solve()
    print(solver.result())

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('filename', type=str)

    args = parser.parse_args()
    main(args.filename)
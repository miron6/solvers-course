from collections import deque
from enum import Enum, IntEnum

from cnf.cnf import ConjunctiveNormalForm

class ClauseStatus(IntEnum):
    TRUE = 1
    FALSE = 0
    UNASSIGNED = -1

class Status(Enum):
    SAT = "SAT"
    UNSAT = "UNSAT"

class Node:
    def __init__(self, variable: int, value: ClauseStatus):
        self.variable = variable
        self.value = value
        self.level = -1
        self.parents = []
        self.children = []
        self.clause = None

    def update_node_to_unassigned(self):
        self.value = ClauseStatus.UNASSIGNED
        self.level = -1
        self.parents = []
        self.children = []
        self.clause = None

    def get_all_parents(self):
        parents = set(self.parents)
        for parent in self.parents:
            for p in parent.get_all_parents():
                parents.add(p)
        return list(parents)

class CDCLSolver:
    _cnf: ConjunctiveNormalForm

    _learnts: set
    _assigned_vars: dict
    _branching_vars: set

    _nodes: dict[int, Node]

    _propogation_history: dict[int, deque]
    _branching_history: dict

    _level: int

    _status: Status

    def __init__(self: 'CDCLSolver', cnf: 'ConjunctiveNormalForm'):
        self._cnf = cnf
        self._learnts = set()
        self._branching_vars = set()
        self._assigned_vars = dict.fromkeys(list(self._cnf.vars), ClauseStatus.UNASSIGNED)
        self._nodes = dict((k, Node(k, ClauseStatus.UNASSIGNED)) for k in list(self._cnf.vars))
        self._propogation_history = {} # level -> propagate variables list
        self._branching_history = {} # level -> branched variable

    def _update_nodes(self, var, clause=None) -> None:
        node: Node = self._nodes[var]
        node.value = self._assigned_vars[var]
        node.level = self._level

        if clause:
            for val in [abs(lit) for lit in clause if abs(lit) != var]:
                node.parents.append(self._nodes[val])
                self._nodes[val].children.append(node)
            node.clause = clause

    def _all_variables_are_assigned(self) -> bool:
        return not any(var for var in self._cnf.vars if self._assigned_vars[var] == ClauseStatus.UNASSIGNED)

    def _compute_value(self, var) -> ClauseStatus:
        value: ClauseStatus = self._assigned_vars[abs(var)]
        if value != ClauseStatus.UNASSIGNED:
            value = value.value ^ (var < 0)
        return ClauseStatus(value)

    def _compute_clause(self, clause) -> ClauseStatus:
        values = list(map(self._compute_value, clause))
        return ClauseStatus.UNASSIGNED if ClauseStatus.UNASSIGNED in values else ClauseStatus(max(values))

    def _is_unit_clause(self, clause) -> tuple[bool, int]:
        values = []
        unassigned_var = None
        for var in clause:
            value = self._compute_value(var)
            values.append(value)
            unassigned_var = var if value == ClauseStatus.UNASSIGNED else unassigned_var
        is_unit = (
            (
                values.count(ClauseStatus.FALSE) == len(clause) - 1 and
                values.count(ClauseStatus.UNASSIGNED) == 1
            ) or (
                len(clause) == 1 and
                values.count(ClauseStatus.UNASSIGNED) == 1
            )
        )
        return is_unit, unassigned_var

    def _unit_propogation_conflict(self) -> None | frozenset[int]:
        while True:
            propagate_queue = deque()
            for clause in [x for x in self._cnf.clauses.union(self._learnts)]:
                value = self._compute_clause(clause)
                if value == ClauseStatus.TRUE:
                    continue
                elif value == ClauseStatus.FALSE:
                    return clause
                else:
                    is_unit, unit_var = self._is_unit_clause(clause)
                    if not is_unit:
                        continue
                    propogation_pair = (unit_var, clause)
                    if propogation_pair not in propagate_queue:
                        propagate_queue.append(propogation_pair)
            if not propagate_queue:
                return None

            for propogation_lit, clause in propagate_queue:
                propogation_var = abs(propogation_lit)
                self._assigned_vars[propogation_var] = ClauseStatus.TRUE if propogation_lit > 0 else ClauseStatus.FALSE
                self._update_nodes(propogation_var, clause=clause)
                try:
                    self._propogation_history[self._level].append(propogation_lit)
                except KeyError:
                    pass

    def _next_recent_assigned(self, clause: frozenset[int], assigned_history: list):
        for v in reversed(assigned_history):
            if v in clause or -v in clause:
                return v, [x for x in clause if abs(x) != abs(v)]

    def _conflict_analysis(self, conflict_clause: frozenset[int]) -> tuple[int, frozenset[int]]:
        if self._level == 0:
            return -1, None

        assign_history = [self._branching_history[self._level] + list(self._propogation_history[self._level])]
        pool_lits = conflict_clause
        done_lits = set()
        curr_level_lits = set()
        prev_level_lits = set()

        while True:
            for lit in pool_lits:
                if self._nodes[abs(lit)].level == self._level:
                    curr_level_lits.add(lit)
                else:
                    prev_level_lits.add(lit)

            if len(curr_level_lits) == 1:
                break
            last_assigned, others = self._next_recent_assigned(curr_level_lits, assign_history)
            done_lits.add(abs(last_assigned))
            curr_level_lits = set(others)

            pool_clause = self._nodes[abs(last_assigned)].clause
            pool_lits = [
                l for l in pool_clause if abs(l) not in done_lits
            ] if pool_clause is not None else []

        learnt = frozenset([l for l in curr_level_lits.union(prev_level_lits)])
        if prev_level_lits:
            level = max([self._nodes[abs(x)].level for x in prev_level_lits])
        else:
            level = self._level - 1

        return level, learnt

    def _backtrack(self, level: int) -> None:
        for _, node in self._nodes.items():
            if node.level <= level:
                node.children[:] = [child for child in node.children if child.level <= level]
            else:
                node.update_node_to_unassigned()
                self._assigned_vars[node.variable] = ClauseStatus.UNASSIGNED

        vars = []
        for var in self._cnf.vars:
            if self._assigned_vars[var] != ClauseStatus.UNASSIGNED and len(self._nodes[var].parents) == 0:
                vars.append(var)
        self._branching_vars = set(vars)

        levels = list(self._propogation_history.keys())
        for l in levels:
            if l <= level:
                continue
            del self._branching_history[l]
            del self._propogation_history[l]

    def _all_assigned_vars_iter(self) -> filter:
        return filter(
            lambda v: v in self._assigned_vars and self._assigned_vars[v] == ClauseStatus.UNASSIGNED,
            self._cnf.vars
        )

    def _pick_branching_variable(self) -> tuple[int, int]:
        var = next(self._all_assigned_vars_iter())
        return var, ClauseStatus.TRUE

    def solve(self) -> None:
        self._level = 0

        while not self._all_variables_are_assigned():
            conflict_clause = self._unit_propogation_conflict()
            if conflict_clause is not None:
                level, learnt = self._conflict_analysis(conflict_clause)
                if level < 0:
                    self._status = Status.UNSAT
                    return
                self._learnts.add(learnt)
                self._backtrack(level)
                self._level = level
            elif self._all_variables_are_assigned():
                break
            else:
                self._level += 1
                var, val = self._pick_branching_variable()
                self._assigned_vars[var] = val
                self._branching_vars.add(var)
                self._branching_history[self._level] = var
                self._propogation_history[self._level] = deque()
                self._update_nodes(var)

        self._status = Status.SAT
        return

    def result(self) -> str:
        if self._status == Status.UNSAT:
            return self._status.value
        res = []
        for k, v in self._assigned_vars.items():
            if v == ClauseStatus.TRUE:
                res.append(str(k))
            else:
                res.append(str(-k))
        return f'{self._status.value} {" ".join(res)}'

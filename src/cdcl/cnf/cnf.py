class ConjunctiveNormalForm:
    _clauses : set[frozenset[int]]
    _vars: set[int]

    _clauses_amount: int
    _vars_amount: int

    def __init__(self, clauses, vars, vars_amount, clauses_amount):
        self._clauses = clauses
        self._vars = vars
        self._vars_amount = vars_amount
        self._clauses_amount = clauses_amount

    @property
    def clauses(self) -> set:
        return self._clauses

    @property
    def vars(self) -> set:
        return self._vars

    @property
    def clauses_amount(self) -> int:
        return self._clauses_amount

    @property
    def vars_amount(self) -> int:
        return self._vars_amount
